import React from 'react'
import 'antd/dist/antd.css';
import { Button, Space } from 'antd';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";




import Office from './pages/office'
import OfficeCreate from './pages/office/create'
import OfficeUpdate from './pages/office/update'
import OfficeDetail from './pages/office/detail'
import OfficeDelete from './pages/office/delete'

import Party from './pages/party'
import PartyCreate from './pages/party/create'
import PartyUpdate from './pages/party/update'
import PartyDetail from './pages/party/detail'
import PartyDelete from './pages/party/delete'

import Candidate from './pages/candidate'
import CandidateCreate from './pages/candidate/create'
import CandidateUpdate from './pages/candidate/update'
import CandidateDetail from './pages/candidate/detail'
import CandidateDelete from './pages/candidate/delete'

import Constituency from './pages/constituency'
import ConstituencyCreate from './pages/constituency/create'
import ConstituencyUpdate from './pages/constituency/update'
import ConstituencyDetail from './pages/constituency/detail'
import ConstituencyDelete from './pages/constituency/delete'

import District from './pages/district'
import DistrictCreate from './pages/district/create'
import DistrictUpdate from './pages/district/update'
import DistrictDetail from './pages/district/detail'
import DistrictDelete from './pages/district/delete'

import State from './pages/state'
import StateCreate from './pages/state/create'
import StateUpdate from './pages/state/update'
import StateDetail from './pages/state/detail'
import StateDelete from './pages/state/delete'

import Town from './pages/town'
import TownCreate from './pages/town/create'
import TownUpdate from './pages/town/update'
import TownDetail from './pages/town/detail'
import TownDelete from './pages/town/delete'

import Township from './pages/township'
import TownshipCreate from './pages/township/create'
import TownshipUpdate from './pages/township/update'
import TownshipDetail from './pages/township/detail'
import TownshipDelete from './pages/township/delete'

import Village from './pages/village'
import VillageCreate from './pages/village/create'
import VillageUpdate from './pages/village/update'
import VillageDetail from './pages/village/detail'
import VillageDelete from './pages/village/delete'

import VillageTracts from './pages/villagetracts'
import VillageTractsCreate from './pages/villagetracts/create'
import VillageTractsUpdate from './pages/villagetracts/update'
import VillageTractsDetail from './pages/villagetracts/detail'
import VillageTractsDelete from './pages/villagetracts/delete'

import Voter from './pages/voter'
import VoterCreate from './pages/voter/create'
import VoterUpdate from './pages/voter/update'
import VoterDetail from './pages/voter/detail'
import VoterDelete from './pages/voter/delete'

import Wards from './pages/wards'
import WardsCreate from './pages/wards/create'
import WardsUpdate from './pages/wards/update'
import WardsDetail from './pages/wards/detail'
import WardsDelete from './pages/wards/delete'

const App = () => {
  return <Router>
    <Space>
      <Button danger><Link to="/office">Office</Link></Button>
      <Button danger><Link to="/party">Party</Link></Button>
      <Button danger><Link to="/candidate">Candidate</Link></Button>
      <Button danger><Link to="/constituency">Constituency</Link></Button>
      <Button danger><Link to="/district">District</Link></Button>
      <Button danger><Link to="/state">State</Link></Button>
      <Button danger><Link to="/town">Town</Link></Button>
      <Button danger><Link to="/township">Township</Link></Button>
      <Button danger><Link to="/village">Village</Link></Button>
      <Button danger><Link to="/village_tracts">VillageTracts</Link></Button>
      <Button danger><Link to="/voter">Voter</Link></Button>
      <Button danger><Link to="/wards">Wards</Link></Button>
    </Space>

    <Switch>
      <Route path="/office" exact={true}><Office /></Route>
      <Route path="/office/create" exact={true}><OfficeCreate /></Route>
      <Route path="/office/edit/:id" exact={true}><OfficeUpdate /></Route>
      <Route path="/office/:id" exact={true}><OfficeDetail /></Route>
      <Route path="/office/delete/:id" exact={true}><OfficeDelete /></Route>

      <Route path="/party" exact={true}><Party /></Route>
      <Route path="/party/create" exact={true}><PartyCreate /></Route>
      <Route path="/party/edit/:id" exact={true}><PartyUpdate /></Route>
      <Route path="/party/:id" exact={true}><PartyDetail /></Route>
      <Route path="/party/delete/:id" exact={true}><PartyDelete /></Route>

      <Route path="/candidate" exact={true}><Candidate /></Route>
      <Route path="/candidate/create" exact={true}><CandidateCreate /></Route>
      <Route path="/candidate/edit/:id" exact={true}><CandidateUpdate /></Route>
      <Route path="/candidate/:id" exact={true}><CandidateDetail /></Route>
      <Route path="/candidate/delete/:id" exact={true}><CandidateDelete /></Route>

      <Route path="/constituency" exact={true}><Constituency /></Route>
      <Route path="/constituency/create" exact={true}><ConstituencyCreate /></Route>
      <Route path="/constituency/edit/:id" exact={true}><ConstituencyUpdate /></Route>
      <Route path="/constituency/:id" exact={true}><ConstituencyDetail /></Route>
      <Route path="/constituency/delete/:id" exact={true}><ConstituencyDelete /></Route>

      <Route path="/district" exact={true}><District /></Route>
      <Route path="/district/create" exact={true}><DistrictCreate /></Route>
      <Route path="/district/edit/:id" exact={true}><DistrictUpdate /></Route>
      <Route path="/district/:id" exact={true}><DistrictDetail /></Route>
      <Route path="/district/delete/:id" exact={true}><DistrictDelete /></Route>

      <Route path="/state" exact={true}><State /></Route>
      <Route path="/state/create" exact={true}><StateCreate /></Route>
      <Route path="/state/edit/:id" exact={true}><StateUpdate /></Route>
      <Route path="/state/:id" exact={true}><StateDetail /></Route>
      <Route path="/state/delete/:id" exact={true}><StateDelete /></Route>

      <Route path="/town" exact={true}><Town /></Route>
      <Route path="/town/create" exact={true}><TownCreate /></Route>
      <Route path="/town/edit/:id" exact={true}><TownUpdate /></Route>
      <Route path="/town/:id" exact={true}><TownDetail /></Route>
      <Route path="/town/delete/:id" exact={true}><TownDelete /></Route>

      <Route path="/township" exact={true}><Township /></Route>
      <Route path="/township/create" exact={true}><TownshipCreate /></Route>
      <Route path="/township/edit/:id" exact={true}><TownshipUpdate /></Route>
      <Route path="/township/:id" exact={true}><TownshipDetail /></Route>
      <Route path="/township/delete/:id" exact={true}><TownshipDelete /></Route>

      <Route path="/village" exact={true}><Village /></Route>
      <Route path="/village/create" exact={true}><VillageCreate /></Route>
      <Route path="/village/edit/:id" exact={true}><VillageUpdate /></Route>
      <Route path="/village/:id" exact={true}><VillageDetail /></Route>
      <Route path="/village/delete/:id" exact={true}><VillageDelete /></Route>

      <Route path="/village_tracts" exact={true}><VillageTracts /></Route>
      <Route path="/village_tracts/create" exact={true}><VillageTractsCreate /></Route>
      <Route path="/village_tracts/edit/:id" exact={true}><VillageTractsUpdate /></Route>
      <Route path="/village_tracts/:id" exact={true}><VillageTractsDetail /></Route>
      <Route path="/village_tracts/delete/:id" exact={true}><VillageTractsDelete /></Route>

      <Route path="/voter" exact={true}><Voter /></Route>
      <Route path="/voter/create" exact={true}><VoterCreate /></Route>
      <Route path="/voter/edit/:id" exact={true}><VoterUpdate /></Route>
      <Route path="/voter/:id" exact={true}><VoterDetail /></Route>
      <Route path="/voter/delete/:id" exact={true}><VoterDelete /></Route>

      <Route path="/wards" exact={true}><Wards /></Route>
      <Route path="/wards/create" exact={true}><WardsCreate /></Route>
      <Route path="/wards/edit/:id" exact={true}><WardsUpdate /></Route>
      <Route path="/wards/:id" exact={true}><WardsDetail /></Route>
      <Route path="/wards/delete/:id" exact={true}><WardsDelete /></Route>
    </Switch>
  </Router>
}

export default App;