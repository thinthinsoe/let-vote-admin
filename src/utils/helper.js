const partyData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const officeData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const candidateData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const constituencyData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const voterData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const wardsData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const townData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const villageData = {
    description: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}
const villagetractsData = {
    status: 'active',
    updated_By: 'admin',
    created_By: 'admin'
}
const districtData = {
    status: 'active',
    updated_By: 'admin',
    created_By: 'admin'
}
const stateData = {
    status: 'active',
    updated_By: 'admin',
    created_By: 'admin'
}
const townshipData = {
    status: 'active',
    updated_by: 'admin',
    created_by: 'admin'
}


export {officeData, 
    partyData, 
    candidateData, 
    constituencyData, 
    voterData, 
    wardsData, 
    townData,
    villageData,
    villagetractsData,
    stateData,
    districtData,
    townshipData
}