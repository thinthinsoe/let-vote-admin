import React from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space } from 'antd'

import api from '../../api'
import {voterData} from '../../utils/helper'
import noti from '../../component/noti'

const { TextArea } = Input;



const CreateFrom = (props) => {
    const onFinish = async(values) => {
        let data = {...values, ...voterData}
        let result = await api().post('/voter', data)
        if(result && result.status === 200){
            props.history.push('/voter')
            noti('success', "Candidate create successful")
        }else{
            noti('error', "Candidate create unsuccessful")
        }
    }
    const onFinishFailed = (err) => {
        { console.log(err, "err") }
    }
    const form = {
        textAlign: 'center'
    }
    return <Card title="Voter Information">
        <Form style={form}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            <Row gutter={16}>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Candidate Name"
                        name="candidate_name" //db name
                    rules={[{ required: true, message: 'Please input Candidate Name!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Election Name"
                        name="election_name" //db name
                        rules={[{ required: true, message: 'Please input election name!' }]}//essential
                    >
                        <TextArea rows={2} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Date of Birth"
                        name="date_of_birth" //db name
                        rules={[{ required: true, message: 'Please input your birthday!' }]}//essential
                    >
                        <TextArea rows={2} placeholder="Select Date" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Race"
                        name="race" //db name
                        rules={[{ required: true, message: 'Please input your Race!' }]}//essential
                    >
                        <TextArea rows={2} placeholder="Enter Here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Religion"
                        name="religion" //db name
                    rules={[{ required: true, message: 'Enter here' }]}
                    >
                    <Input />
                    </Form.Item>
                </Col>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="NRIC No."
                        name="nric_no" //db name
                    rules={[{ required: true, message: 'Please input your NRIC Number!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Permanent Address"
                        name="permanent_address" //db name
                        rules={[{ required: true, message: 'Please input your address!' }]}//essential
                    >
                        <TextArea rows={2} placeholder="Enter Here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit">
                                Create
                            </Button>
                            <Button type="danger" onClick={() => { props.history.push('/voter') }}>
                                Cancel
                            </Button>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    </Card>
}

export default withRouter(CreateFrom);