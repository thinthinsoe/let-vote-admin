import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'

const Voter = (props) => {
    const [state, setState] = useState([])
    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/voter')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData();
    }, []);
    const columns = [
        {
            title: 'Voter Name',
            dataIndex: 'voter_name',
            key: 'voter_name'
        },
        {
            title: 'NRIC NO',
            dataIndex: 'nric_no',
            key: 'nric_no'
        },
        {
            title: 'Date Of Birth',
            dataIndex: 'date_of_birth',
            key: 'date_of_birth'
        },
        {
            title: 'Race',
            dataIndex: 'race',
            key: 'race'
        },
        {
            title: 'Religion',
            dataIndex: 'religion',
            key: 'religion'
        },
        {
            title: 'Permanent Address',
            dataIndex: 'permanent_address',
            key: 'permanent_address'
        }
    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        {console.log(state, "state")}
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/voter/create')}>
            Create New Voter
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(Voter);