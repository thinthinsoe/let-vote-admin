import React,{useState, useEffect} from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button} from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'

const Party = (props) => {
    const [state, setState] = useState([])
    useEffect( () => {
        async function fetchData(){
          const result = await api().get('/party')
          console.log(result, "result")
          result && result.status === 200 && setState(result.data)
          
            // const res = await fetch('http://localhost:4000/Party').then(res => res.json())
            // res && setState(res)

        }
        fetchData();
    }, []);
    const columns = [
    {
      title: 'Party Name',
      dataIndex: 'party_name',
      key: 'party_name'
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description'
    },
    {
      title: 'Remark',
      dataIndex: 'remark',
      key: 'remark'
    }
    // {
    //   title: 'Status',
    //   dataIndex: 'Status',
    //   key: 'Status'
    // }

  ]
  const button = {
    margin: '20px 0px'
  }
  return <div>
    <Button
      type="primary"
      shape="round"
      icon={<PlusOutlined />}
      style={button} size={"large"}
      onClick={() => props.history.push('/party/create')}>
      Create New Party
    </Button>
    <Table columns={columns} dataSource={state} />
  </div>
}

export default withRouter(Party);