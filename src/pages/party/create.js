import React from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space } from 'antd'

import api from '../../api'
import {partyData} from '../../utils/helper'
import noti from '../../component/noti'

const { TextArea } = Input;



const CreateFrom = (props) => {
    const onFinish = async(values) => {
        let data = {...values, ...partyData}
        let result = await api().post('/party', data)
        if(result && result.status === 200){
            props.history.push('/party')
            noti('success', "Party create successful")
        }else{
            noti('error', "Party create unsuccessful")
        }
    }
    const onFinishFailed = (err) => {
        { console.log(err, "err") }
    }
    const form = {
        textAlign: 'center'
    }
    return <Card title="Party Information">
        <Form
            style = {form}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >

            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Party Name"
                        name="party_name" //db name
                        rules={[{ required: true, message: 'Please input your party name!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
            <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Office"
                        name="office_id" //db name
                        rules={[{ required: true, message: 'Please input your office!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Constituency Name"
                        name="constituency_name" //db name
                        rules={[{ required: true, message: 'Please input your constituency name!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Status"
                        name="status" //db name
                    //rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <TextArea rows={3} placeholder="status" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                            <Button type="danger" onClick={() => { props.history.push('/party') }}>
                                Cancel
                            </Button>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    </Card>
}

export default withRouter(CreateFrom);