import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'

const Village = (props) => {
    const [state, setState] = useState([])
    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/village')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData();
    }, []);
    const columns = [
        {
            title: 'Village Code',
            dataIndex: 'village_code',
            key: 'village_code'
        },
        {
            title: 'Village Name',
            dataIndex: 'village_name',
            key: 'village_name'
        },
        {
            title: 'Remark',
            dataIndex: 'remark',
            key: 'remark'
        }
    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        {console.log(state, "state")}
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/village/create')}>
            Create New Village
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(Village);