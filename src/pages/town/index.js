import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'

const Town = (props) => {
    const [state, setState] = useState([])
    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/town')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData();
    }, []);
    const columns = [
        {
            title: 'Town Code',
            dataIndex: 'town_code',
            key: 'town_code'
        },
        {
            title: 'Town Name',
            dataIndex: 'town_name',
            key: 'town_name'
        },
        {
            title: 'Remark',
            dataIndex: 'remark',
            key: 'remark'
        }
    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        {console.log(state, "state")}
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/town/create')}>
            Create New Town
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(Town);