import React, {useState, useEffect} from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space } from 'antd'


import api from '../../api'

const { TextArea } = Input;


const DetailForm = (props) => {

    const id = props.match.params.id

    const [state, setState] = useState(false)
    useEffect( () => {
        async function fetchData (){
            let result = await api().get(`/state/${id}`)
            console.log(result ,"result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData()
    }, [])

    const form = {
        textAlign: 'center'
    }
    return <div>
        {console.log(state, "state")}
        {
            state ? <Card title="State Information" style={form}>
            <Form style={form}
                layout="vertical"
                initialValues={state}
            >
    
                <Row gutter={16}>
                    <Col sm={24} md={12} lg={6}>
                        <Form.Item
                            label="State Code"
                            name="state_code" //db name
                        >
                            <Input readOnly/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col sm={24} md={12} lg={6}>
                        <Form.Item
                            label="State Region"
                            name="state_region" //db name
                        >
                            <Input readOnly/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col sm={24} md={12} lg={12}>
                        <Form.Item
                            label="Remark"
                            name="remark" //db name
                        >
                            <Input readOnly/>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col sm={24} md={12} lg={12}>
                        <Form.Item>
                            <Space>
                                <Button type="primary" htmlType="submit" onClick={() => props.history.push(`/state/edit/${id}`)}>
                                    Edit
                                </Button>
                                <Button type="danger" htmlType="submit" onClick={() => props.history.push('/state')}>
                                    Close
                                </Button>
                            </Space>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Card> : <div>Loading...</div>
        }
    </div>
}

export default withRouter(DetailForm);