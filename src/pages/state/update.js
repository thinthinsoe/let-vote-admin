import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space } from 'antd'


import api from '../../api'
import { stateData } from '../../utils/helper';
import noti from '../../component/noti'

const { TextArea } = Input;

const UpdateForm = (props) => {

    const id = props.match.params.id

    const [state, setState] = useState(false)
    useEffect(() => {
        async function fetchData() {
            let result = await api().get(`/state/${id}`)
            //console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData()
    }, [])
    const onFinish = async (values) => {
        let data = { ...values, Updated_By: stateData.Updated_By }
         console.log(data, "data")
        let result = await api().put(`/state/${id}`, data)
        if (result && result.status === 200) {
            props.history.push('/state')
            noti('success', "State update successful")
        } else {
            noti('error', "State update fail")
        }
    }
    const onFinishFailed = (err) => {
        console.log(err, "err")
    }
    const form = {
        textAlign: 'center'
    }
    return <div>
        {
            state ? <Card title="State Information">
                <Form style={form}
                    layout="vertical"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    initialValues={state}
                >
                    <Row>
                        <Col sm={24} md={12} lg={12}>
                            <Form.Item
                                name="state_id" //db name
                                hidden={true}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col sm={24} md={12} lg={6}>
                            <Form.Item
                                label="State Code"
                                name="state_code" //db name
                                rules={[{ required: true, message: 'Please enter State Code!' }]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col sm={24} md={12} lg={6}>
                            <Form.Item
                                label="State Region"
                                name="state_region" //db name
                                rules={[{ required: true, message: 'Please input State Region!' }]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={24} md={12} lg={12}>
                            <Form.Item
                                label="Remark"
                                name="remark" //db name
                                rules={[{ required: true, message: 'Please input Remark!' }]}
                            >
                                <TextArea rows={3} placeholder="Enter here" allowClear />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col sm={24} md={12} lg={12}>
                            <Form.Item>
                                <Space>
                                    <Button type="primary" htmlType="submit">
                                        Update
                                </Button>
                                    <Button type="danger" onClick={() => { props.history.push('/state') }}>
                                        Cancel
                                </Button>
                                </Space>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Card> : <div>Loading...</div>
        }
    </div>
}


export default withRouter(UpdateForm);