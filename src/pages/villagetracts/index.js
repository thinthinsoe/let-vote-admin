import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'

const Village_Tracts = (props) => {
    const [state, setState] = useState([])
    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/village_tracts')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData();
    }, []);
    const columns = [
        {
            title: 'Village Tracts Code',
            dataIndex: 'village_tracts_code',
            key: 'village_tracts_code'
        },
        {
            title: 'Village Tracts Name',
            dataIndex: 'village_tracts_name',
            key: 'village_tracts_name'
        },
        {
            title: 'Remark',
            dataIndex: 'remark',
            key: 'remark'
        }
    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        {console.log(state, "state")}
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/village_tracts/create')}>
            Create New VillageTract
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(Village_Tracts);