import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom'
//import logo from './logo.svg';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import { Table, Button} from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'
//import { OmitProps } from 'antd/lib/transfer/ListBody';

const Office = (props) => {
  const [state, setState] = useState([])
  useEffect(() => {
    async function fetchData() {
            const result = await api().get('/office')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
    }
    fetchData();
  }, []);
  const columns = [
    {
      title: 'Office Name',
      dataIndex: 'office_name',
      key: 'office_name'
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address'
    },
    {
      title: 'PONO',
      dataIndex: 'pono',
      key: 'pono'
    },
    {
      title: 'PHNO1',
      dataIndex: 'phno1',
      key: 'phno1'
    },
    {
      title: 'PHNO2',
      dataIndex: 'phno2',
      key: 'phno2'
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    },
    {
      title: 'Building Type',
      dataIndex: 'building_type',
      key: 'building_type'
    },
    {
      title: 'Own or Rental',
      dataIndex: 'Own_or_Rental',
      key: 'own_or_rental'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status'
    }

  ]
  const button = {
    margin: '20px 0px'
  }
  return <div>
    <Button
      type="primary"
      shape="round"
      icon={<PlusOutlined />}
      style={button} size={"large"}
      onClick={() => props.history.push('/office/create')}>
      Create New Office
    </Button>
    <Table columns={columns} dataSource={state} />
  </div>
}

export default withRouter(Office);
