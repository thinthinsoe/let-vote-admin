import React from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space, Radio } from 'antd'

import api from '../../api'
import {officeData} from '../../utils/helper'
import noti from '../../component/noti'

const { TextArea } = Input;



const CreateFrom = (props) => {
    const onFinish = async(values) => {
        let data = {...values, ...officeData}
        let result = await api().post('/office', data)
        if(result && result.status === 200){
            props.history.push('/office')
            noti('success', "Office create successful")
        }else{
            noti('error', "Office create unsuccessful")
        }
    }
    const onFinishFailed = (err) => {
        { console.log(err, "err") }
    }
    const form = {
        textAlign: 'center'
    }
    return <Card title="Office Information">
        <Form style={form}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >

            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Office Name"
                        name="office_name" //db name
                        rules={[{ required: true, message: 'Please input your office name!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Address"
                        name="address" //db name
                        rules={[{ required: true, message: 'Please input your address!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Village"
                        name="village_id" //db name
                        rules={[{ required: true, message: 'Please input your Village!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Village Tract"
                        name="village_tracts_id" //db name
                        rules={[{ required: true, message: 'Please input your Village tract!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Wards"
                        name="wards_id" //db name
                        rules={[{ required: true, message: 'Please input your Ward!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Town"
                        name="town_id" //db name
                        rules={[{ required: true, message: 'Please input your Town!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Township"
                        name="township_id" //db name
                        rules={[{ required: true, message: 'Please input your Township!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="District"
                        name="district_id" //db name
                        rules={[{ required: true, message: 'Please input your District!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="State"
                        name="state_id" //db name
                        rules={[{ required: true, message: 'Please input your State!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="PONO"
                        name="pono" //db name
                        rules={[{ required: true, message: 'Please input your PONO!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="PhNo.1"
                        name="phno1" //db name
                    rules={[{ required: true, message: 'Please input PHNO1!' }]}
                    >
                        <TextArea rows={2} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="PhNo.2"
                        name="phno2" //db name
                    rules={[{ required: true, message: 'Please input PHNO2!' }]}
                    >
                        <TextArea rows={2} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Email"
                        name="email" //db name
                        rules={[{ required: true, message: 'Please input your email!' }]}
                    >
                        <TextArea rows={2} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Building Type"
                        name="building_type" //db name
                        rules={[{ required: true, message: 'Please input your email!' }]}
                    >
                        <TextArea rows={2} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                        <Radio value={1}>Own</Radio>
                        <Radio value={2}>Rental</Radio>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                            <Button type="danger" onClick={() => { props.history.push('/office') }}>
                                Cancel
                            </Button>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    </Card>
}

export default withRouter(CreateFrom);