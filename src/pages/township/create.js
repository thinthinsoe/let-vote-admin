import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space, Select } from 'antd'

import api from '../../api'
import {townshipData} from '../../utils/helper'
import noti from '../../component/noti'

const { TextArea } = Input;
const { Option } = Select;


const CreateFrom = (props) => {

    const [stateList, setStateList] = useState([])
    const [state, setState] = useState([])

    useEffect(() => {
        async function fetchData() {
            const stateData = await api().get('/state')
            console.log(stateData, "stateData")
            stateData && stateData.status === 200 && setState(stateData.data)
        }
        fetchData()
    }, [])

    useEffect(() => {
        async function fetchData() {
            const stateData = await api().get('/district')
            console.log(stateData, "stateData")
            stateData && stateData.status === 200 && setStateList(stateData.data)
        }
        fetchData()
    }, [])


    const onFinish = async(values) => {
        let data = {...values, ...townshipData}
        let result = await api().post('/township', data)
        if(result && result.status === 200){
            props.history.push('/township')
            noti('success', "Township create successful")
        }else{
            noti('error', "Township create unsuccessful")
        }
    }
    const onFinishFailed = (err) => {
        console.log(err, "err")
    }
    const onChange = (value) => {
        console.log(value, "value")
    }
    const form = {
        textAlign: 'center'
    }
    return <Card title="Township Information">
        <Form style={form}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >

            <Row gutter={16}>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Township Code"
                        name="township_code" //db name
                    rules={[{ required: true, message: 'Please enter Township Code!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Township Name"
                        name="township_name" //db name
                    rules={[{ required: true, message: 'Please input Township Name!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            {/* <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="State Region"
                        name="state_id" //db name
                        rules={[{ required: true, message: 'Please select District Name!' }]}//essential
                    >
                        <Select
                            showSearch
                            optionFilterProp="children"
                            onChange={onChange}
                        >
                            {
                                state && state.map((item) => {
                                    return <Option value={item.state_id}>
                                        {item.state_region}
                                    </Option>
                                })
                            }

                        </Select>
                    </Form.Item>
                </Col>
            </Row> */}
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="District Name"
                        name="district_id" //db name
                        rules={[{ required: true, message: 'Please select District Name!' }]}//essential
                    >
                        <Select
                            showSearch
                            optionFilterProp="children"
                            onChange={onChange}
                        >
                            {
                                stateList && stateList.map((item) => {
                                    return <Option value={item.district_id}>
                                        {item.district_name}
                                    </Option>
                                })
                            }

                        </Select>
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Remark"
                        name="remark" //db name
                    rules={[{ required: true, message: 'Please input Remark!' }]}
                    >
                        <TextArea rows={3} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit">
                                Create
                            </Button>
                            <Button type="danger" onClick={() => { props.history.push('/township') }}>
                                Cancel
                            </Button>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    </Card>
}

export default withRouter(CreateFrom);