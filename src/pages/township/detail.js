import React, {useState, useEffect} from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space, Select } from 'antd'


import api from '../../api'

const { TextArea } = Input;
const { Option } = Select;


const DetailForm = (props) => {

    const id = props.match.params.id
    const [state, setState] = useState(false)
    const [stateList, setStateList] = useState([])

    useEffect( () => {
        async function fetchData (){
            let result = await api().get(`/township/${id}`)
            console.log(result ,"result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData()
    }, [])
    
    useEffect(() => {
        async function fetchData() {
            const stateData = await api().get('/district')
            console.log(stateData, "stateData")
            stateData && stateData.status === 200 && setStateList(stateData.data)
        }
        fetchData()
    }, [])
    

    const form = {
        textAlign: 'center'
    }
    return <div>
        {console.log(state, "state")}
        {
            state ? <Card title="Township Information" style={form}>
            <Form style={form}
                layout="vertical"
                initialValues={state}
            >
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="District Name"
                        name="district_id" //db name
                        //rules={[{ required: true, message: 'Please select District Name!' }]}//essential
                    >       
                        <Select>
                            {
                                stateList && stateList.map((item) => {
                                    return <Option value={item.district_id}>
                                        {item.district_name}
                                    </Option>
                                })
                            }
                        </Select>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Township Code"
                        name="township_code" //db name
                    //rules={[{ required: true, message: 'Please enter Township Code!' }]}
                    >
                        <Input readOnly/>
                    </Form.Item>
                </Col>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Township Name"
                        name="township_name" //db name
                    //rules={[{ required: true, message: 'Please input Township Name!' }]}
                    >
                        <Input readOnly/>
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Remark"
                        name="remark" //db name
                    >
                        <TextArea rows={3} placeholder="Enter here" allowClear readOnly/>
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit" onClick={() => props.history.push(`/township/edit/${id}`)}>
                                Edit
                            </Button>
                            <Button type="danger" onClick={() => { props.history.push('/township') }}>
                                Close
                            </Button>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
            </Form>
        </Card> : <div>Loading...</div>
        }
    </div>
}

export default withRouter(DetailForm);