import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button, Space,  Popconfirm} from 'antd'
import { PlusOutlined, EyeOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import api from '../../api'

const Township = (props) => {
    const [state, setState] = useState([])
    const [reload, setReload] = useState(false)

    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/township')
            //console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData();
    }, [reload]);
    const confirm = async(id) => {
        const result = await api().delete(`township/${id}`)
        console.log(result, "result")
        result && result.data === "DELETED" && setReload(!reload)
    }
    const columns = [
        {
            title: 'Township Code',
            dataIndex: 'township_code',
            key: 'township_code'
        },
        {
            title: 'District Name',
            dataIndex: 'district_name',
            key: 'district_name'
        },
        {
            title: 'Township Name',
            dataIndex: 'township_name',
            key: 'township_name'
        },
        {
            title: 'Remark',
            dataIndex: 'remark',
            key: 'remark'
        },
        {
            title: 'Action',
            render: (text, record) => {
                let id = record.township_id
                return <Space>
                    <a href onClick={ () => props.history.push(`/township/${id}`)}><EyeOutlined /></a>
                    <a href onClick={ () => props.history.push(`/township/edit/${id}`)}><EditOutlined /></a>
                    <Popconfirm
                        onConfirm={() => confirm(id)}
                        title = "Are you sure?"
                        okText = "yes"
                        cancelText = "No"
                    >
                        <a href><DeleteOutlined /></a>
                    </Popconfirm>
                </Space>
            }
        }
    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        {console.log(state, "state")}
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/township/create')}>
            Create New Township
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(Township);