import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'

const Candidate = (props) => {
    const [state, setState] = useState([])
    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/candidate')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData();
    }, []);
    const columns = [
        {
            title: 'Candidate Name',
            dataIndex: 'candidate_name',
            key: 'candidate_name'
        },
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image'
        },
        {
            title: 'Election Name',
            dataIndex: 'election_name',
            key: 'election_name'
        },
        {
            title: 'Date Of Birth',
            dataIndex: 'date_of_birth',
            key: 'date_of_birth'
        },
        {
            title: 'Race',
            dataIndex: 'race',
            key: 'race'
        },
        {
            title: 'Religion',
            dataIndex: 'religion',
            key: 'religion'
        },
        {
            title: 'NRIC NO',
            dataIndex: 'nric_no',
            key: 'nric_no'
        },
        {
            title: 'Education',
            dataIndex: 'education',
            key: 'education'
        },
        {
            title: 'Occupation',
            dataIndex: 'occupation',
            key: 'occupation'
        },
        {
            title: 'Permanent Address',
            dataIndex: 'permanent_address',
            key: 'permanent_address'
        },
        {
            title: 'Why to be a candidate',
            dataIndex: 'why_to_be_a_candidate',
            key: 'why_to_be_a_candidate'
        }
        // {
        //   title: 'Status',
        //   dataIndex: 'Status',
        //   key: 'Status'
        // }

    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/candidate/create')}>
            Create New Candidate
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(Candidate);