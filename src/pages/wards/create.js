import React from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space } from 'antd'

import api from '../../api'
import {townData} from '../../utils/helper'
import noti from '../../component/noti'

const { TextArea } = Input;



const CreateFrom = (props) => {
    const onFinish = async(values) => {
        let data = {...values, ...townData}
        let result = await api().post('/wards', data)
        if(result && result.status === 200){
            props.history.push('/wards')
            noti('success', "Ward create successful")
        }else{
            noti('error', "Ward create unsuccessful")
        }
    }
    const onFinishFailed = (err) => {
        console.log(err, "err")
    }
    const form = {
        textAlign: 'center'
    }
    return <Card title="Ward Information">
        <Form style={form}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >

            <Row gutter={16}>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Ward Code"
                        name="wards_code" //db name
                    rules={[{ required: true, message: 'Please enter Ward Code!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Wards Name"
                        name="wards_name" //db name
                    rules={[{ required: true, message: 'Please input Wards Name!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="State Region"
                        name="state_id" //db name
                    rules={[{ required: true, message: 'Please input State Region!' }]}
                    >
                        <TextArea rows={3} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="District"
                        name="district_id" //db name
                    rules={[{ required: true, message: 'Please input District!' }]}
                    >
                        <TextArea rows={3} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Township"
                        name="township_id" //db name
                    rules={[{ required: true, message: 'Please input Township!' }]}
                    >
                        <TextArea rows={3} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Towns"
                        name="town_id" //db name
                    rules={[{ required: true, message: 'Please input Town!' }]}
                    >
                        <TextArea rows={3} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Remark"
                        name="remark" //db name
                    rules={[{ required: true, message: 'Please input Remark!' }]}
                    >
                        <TextArea rows={3} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit">
                                Create
                            </Button>
                            <Button type="danger" onClick={() => { props.history.push('/wards') }}>
                                Cancel
                            </Button>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    </Card>
}

export default withRouter(CreateFrom);