import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button, Space,  Popconfirm } from 'antd'
import { PlusOutlined, EyeOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';


import api from '../../api'

const District = (props) => {

    const [state, setState] = useState([])
    const [reload, setReload] = useState(false)

    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/district')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData();
    }, [reload]);

    const confirm = async(id) => {
        const result = await api().delete(`district/${id}`)
        console.log(result, "result")
        result && result.data === "DELETED" && setReload(!reload)
    }

    const columns = [
        {
            title: 'State Region',
            dataIndex: 'state_region',
            key: 'state_region'
        },
        {
            title: 'District Code',
            dataIndex: 'district_code',
            key: 'district_code'
        },
        {
            title: 'District Name',
            dataIndex: 'district_name',
            key: 'district_name'
        },
        {
            title: 'Remark',
            dataIndex: 'remark',
            key: 'remark'
        },
        {
            title: 'Action',
            render: (text, record) => {
                let id = record.district_id
                return <Space>
                    <a href onClick={ () => props.history.push(`/district/${id}`)}><EyeOutlined /></a>
                    <a href onClick={ () => props.history.push(`/district/edit/${id}`)}><EditOutlined /></a>
                    <Popconfirm
                        onConfirm={() => confirm(id)}
                        title = "Are you sure?"
                        okText = "yes"
                        cancelText = "No"
                    >
                        <a href><DeleteOutlined /></a>
                    </Popconfirm>
                </Space>
            }
        }
    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        {console.log(state, "state")}
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/district/create')}>
            Create New District
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(District);