import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space, Select } from 'antd'


import api from '../../api'
import { stateData } from '../../utils/helper';
import noti from '../../component/noti'

const { TextArea } = Input;
const { Option } = Select;


const UpdateForm = (props) => {

    const id = props.match.params.id

    const [state, setState] = useState(false)
    const [stateList, setStateList] = useState([])

    useEffect(() => {
        async function fetchData() {
            let result = await api().get(`/district/${id}`)
            //console.log(result, "result")
            result && result.status === 200 && setState(result.data)
        }
        fetchData()
    }, [])


    useEffect(() => {
        async function fetchData() {
            const stateData = await api().get('/state')
            console.log(stateData, "stateData")
            stateData && stateData.status === 200 && setStateList(stateData.data)
        }
        fetchData()
    }, [])


    const onFinish = async (values) => {
        let data = { ...values, updated_by: stateData.updated_by }
         console.log(data, "data")
        let result = await api().put(`/district/${id}`, data)
        if (result && result.status === 200) {
            props.history.push('/district')
            noti('success', "District update successful")
        } else {
            noti('error', "District update fail")
        }
    }
    const onFinishFailed = (err) => {
        console.log(err, "err")
    }
    const onChange = (value) => {
        console.log(value, "value")
    }
    const form = {
        textAlign: 'center'
    }
    return <div>
        {
            state ? <Card title="District Information">
                <Form style={form}
                    layout="vertical"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    initialValues={state}
                >   <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="State Region"
                        name="state_id" //db name
                       // rules={[{ required: true, message: 'Please select District Name!' }]}//essential
                    >
                        <Select
                            showSearch
                            optionFilterProp="children"
                            onChange={onChange}
                        >
                            {
                                stateList && stateList.map((item) => {
                                    return <Option value={item.state_id}>
                                        {item.state_region}
                                    </Option>
                                })
                            }

                        </Select>
                    </Form.Item>
                </Col>
            </Row>
                    <Row>
                        <Col sm={24} md={12} lg={12}>
                            <Form.Item
                                name="district_id" //db name
                                hidden={true}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col sm={24} md={12} lg={6}>
                            <Form.Item
                                label="District Code"
                                name="district_code" //db name
                                rules={[{ required: true, message: 'Please enter District Code!' }]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                        <Col sm={24} md={12} lg={6}>
                            <Form.Item
                                label="District Name"
                                name="district_name" //db name
                                rules={[{ required: true, message: 'Please input District Name!' }]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={24} md={12} lg={12}>
                            <Form.Item
                                label="Remark"
                                name="remark" //db name
                                rules={[{ required: true, message: 'Please input Remark!' }]}
                            >
                                <TextArea rows={3} placeholder="Enter here" allowClear />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col sm={24} md={12} lg={12}>
                            <Form.Item>
                                <Space>
                                    <Button type="primary" htmlType="submit">
                                        Update
                                </Button>
                                    <Button type="danger" onClick={() => { props.history.push('/district') }}>
                                        Cancel
                                </Button>
                                </Space>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Card> : <div>Loading...</div>
        }
    </div>
}


export default withRouter(UpdateForm);