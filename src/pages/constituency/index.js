import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Table, Button } from 'antd'
import { PlusOutlined } from '@ant-design/icons';
import api from '../../api'

const Constituency = (props) => {
    const [state, setState] = useState([])
    useEffect(() => {
        async function fetchData() {
            const result = await api().get('/constituency')
            console.log(result, "result")
            result && result.status === 200 && setState(result.data)
          
        }
        fetchData();
    }, []);
    const columns = [
        {
            title: 'Constituency Name',
            dataIndex: 'constituency_name',
            key: 'constituency_name'
        },
        {
            title: 'Election Name',
            dataIndex: 'election_name',
            key: 'election_name'
        },
        {
            title: 'Code1',
            dataIndex: 'code1',
            key: 'code1'
        },
        {
            title: 'Code2',
            dataIndex: 'code2',
            key: 'code2'
        }
    ]
    const button = {
        margin: '20px 0px'
    }
    return <div>
        <Button
            type="primary"
            shape="round"
            icon={<PlusOutlined />}
            style={button} size={"large"}
            onClick={() => props.history.push('/constituency/create')}>
            Create New Constituency
        </Button>
        <Table columns={columns} dataSource={state} />
    </div>
}

export default withRouter(Constituency);