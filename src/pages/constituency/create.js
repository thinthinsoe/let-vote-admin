import React from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Input, Button, Row, Col, Card, Space } from 'antd'

import api from '../../api'
import {constituencyData} from '../../utils/helper'
import noti from '../../component/noti'

const { TextArea } = Input;



const CreateFrom = (props) => {
    const onFinish = async(values) => {
        let data = {...values, ...constituencyData}
        let result = await api().post('/constituency', data)
        if(result && result.status === 200){
            props.history.push('/constituency')
            noti('success', "Constituency create successful")
        }else{
            noti('error', "Constituency create unsuccessful")
        }
    }
    const onFinishFailed = (err) => {
        console.log(err, "err")
    }
    const form = {
        textAlign: 'center'
    }
    return <Card title="Constituency Information">
        <Form style={form}
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >

            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Constituency Name"
                        name="constituency_name" //db name
                        rules={[{ required: true, message: 'Please input your constituency name!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Election Name"
                        name="election_name" //db name
                        rules={[{ required: true, message: 'Please input your election name!' }]}//essential
                    >
                        <TextArea rows={2} placeholder="Enter here" allowClear />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Code1"
                        name="code1" //db name
                    rules={[{ required: true, message: 'Please input Code1!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
                <Col sm={24} md={12} lg={6}>
                    <Form.Item
                        label="Code2"
                        name="code2" //db name
                    rules={[{ required: true, message: 'Please input Code2!' }]}
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Village"
                        name="village_id" //db name
                        rules={[{ required: true, message: 'Please input your Village!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Village Tract"
                        name="village_tracts_id" //db name
                        rules={[{ required: true, message: 'Please input your Village tract!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Wards"
                        name="wards_id" //db name
                        rules={[{ required: true, message: 'Please input your Ward!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Town"
                        name="town_id" //db name
                        rules={[{ required: true, message: 'Please input your Town!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="Township"
                        name="township_id" //db name
                        rules={[{ required: true, message: 'Please input your Township!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="District"
                        name="district_id" //db name
                        rules={[{ required: true, message: 'Please input your District!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item
                        label="State"
                        name="state_id" //db name
                        rules={[{ required: true, message: 'Please input your State!' }]}//essential
                    >
                        <Input />
                    </Form.Item>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col sm={24} md={12} lg={12}>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit">
                                Create
                            </Button>
                            <Button type="danger" onClick={() => { props.history.push('/constituency') }}>
                                Cancel
                            </Button>
                        </Space>
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    </Card>
}

export default withRouter(CreateFrom);