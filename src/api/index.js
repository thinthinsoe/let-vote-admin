const axios = require('axios').default;

const instance = () => {
    return axios.create({
        baseURL: 'http://localhost:5000/',
        timeout: 1000,
      });
}


export default instance;