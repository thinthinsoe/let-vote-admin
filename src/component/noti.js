import { notification } from 'antd';

const openNotificationWithIcon = (type, message) => {
  notification[type]({
    message: type.toUpperCase(),
    description: message
  });
};

export default openNotificationWithIcon;